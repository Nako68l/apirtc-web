import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { CallPage } from '../call/call';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  numberToCall: number
  apiRTC
  ua
  webRTCClient
  callId

  constructor(public navCtrl: NavController, private toastCtrl: ToastController) {



    //UA REGISTRATION -----------------------------------------------------------------------------------------------------------------
    // this.ua = new this.apiRTC.UserAgent({
    //   uri: 'apzkey:0bb7d9098b476d90061a8ca8a657a5cc',
    //   apiRTCMediaDeviceDetectionEnabled: true, //This option can be use on Chrome/Android or Safari as event ondevicechange is not supported on these browsers
    //   apiRTCMediaDeviceDetectionDelay: 7000
    // });
    // this.ua.register({
    //   // id: 1 // OPTIONAL // This is used for setting userId
    // }).then(function (session) {
    //   // ok
    //   console.log("Registration OK");
    //   console.log(session);
    // }.bind(this)).catch(function (error) {
    //   console.log("Registration error");
    //   console.log(error);
    // })
    // //   this.ua.on('mediaDeviceChanged', function (invitation) {
    // //     //Listen for mediaDeviceChanged event to update devices list
    // //     console.log('mediaDeviceChanged');
    // //     var res = this.ua.getUserMediaDevices();
    // //     console.log(res);
    // //     // updateDeviceList (res); //This is the function to display the devices list in the page. Check our sample source code for more details
    // // });
    // console.log(this.ua.getUserMediaDevices());

    //-----------------------------------------------------------------------------------------------------------------------------------
  }

  ionViewDidLoad() {
    this.apiRTC = window['apiRTC']
    this.apiRTC.init({
      apiKey: "0bb7d9098b476d90061a8ca8a657a5cc",
      onReady: this.onReady.bind(this)
    });
    console.log('COnstH', this.apiRTC);
  }

  onReady(e) {
    console.log('Session created with sessionId' + this.apiRTC.session.apiCCId);
    console.log('CallId:' + e.detail.callId);

    this.apiRTC.addEventListener('incomingCall', this.incomingCallHandler.bind(this));
    this.apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler.bind(this))
    this.apiRTC.addEventListener("remoteStreamAdded", this.remoteStreamAddedHandler.bind(this));

    this.apiRTC.addEventListener("webRTCClientCreated", this.webRTCClientCreatedHandler.bind(this));
    this.apiRTC.addEventListener("hangup", this.hangupHandler.bind(this));

    this.webRTCClient = this.apiRTC.session.createWebRTCClient({
      status: "status",
      command: "command"
    });
  }

  callFriend() {
    // let contact = this.apiRTC.session.getOrCreateContact(this.numberToCall);
    // let call = contact.call()
    if (!this.numberToCall) {
      this.toastCtrl.create({
        message: 'Number cannot be empty',
        duration: 3000,
        position: 'top'
      }).present();
    } else {
      this.webRTCClient.call(this.numberToCall)
      this.navCtrl.push(CallPage, { webRTCClient: this.webRTCClient, apiRTC: this.apiRTC })
    }
  }

  incomingCallHandler(e) {
    console.log("MAIN - incomingCall");
    console.log(e);
    // this.apiRTC.removeEventListener("userMediaSuccess", this.userMediaSuccessHandler.bind(this))
    // this.apiRTC.removeEventListener("remoteStreamAdded", this.remoteStreamAddedHandler.bind(this));
    this.navCtrl.push(CallPage, { webRTCClient: this.webRTCClient, apiRTC: this.apiRTC })

    //==============================
    // ACCEPT CALL INVITATION
    //==============================
    // invitation.accept()
    //   .then(function (call) {
    //     console.log('call accepted');
    //     // setCallListeners(call);
    //     // addHangupButton(call.getId());
    //   });
    // Display hangup button
    // document.getElementById('hangup').style.display = 'inline-block';
  }


  userMediaSuccessHandler(e) {
    console.log("userMediaSuccessHandler e.detail.callId :" + e.detail.callId);
    console.log("userMediaSuccessHandler e.detail.callType :" + e.detail.callType);
    console.log("userMediaSuccessHandler e.detail.remoteId :" + e.detail.remoteId);

    //Adding local Stream in Div. Video is muted

    //You can decide to manage your own stream display function or use the integrated one of ApiRTC
    /*
        addStreamInDiv(e.detail.stream, e.detail.callType, "mini", 'miniElt-' + e.detail.callId,
                       {width : "160px", height : "120px"}, true);
    */
    console.log(this.webRTCClient);
    console.log(e);

    this.webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "mini", 'miniElt-' + e.detail.callId,
      { width: "128px", height: "96px" }, true);
  }

  remoteStreamAddedHandler(e) {
    console.log("remoteStreamAddedHandler, e.detail.callId :" + e.detail.callId);
    console.log("remoteStreamAddedHandler, e.detail.callType :" + e.detail.callType);
    console.log("userMediaSuccessHandler e.detail.remoteId :" + e.detail.remoteId);

    //Adding Remote Stream in Div. Video is not muted

    //You can decide to manage your own stream display function or use the integrated one of ApiRTC
    /*
        addStreamInDiv(e.detail.stream, e.detail.callType, "remote", 'remoteElt-' + e.detail.callId,
                       {width : "640px", height : "480px"}, false);
    */

    this.webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "remote", 'remoteElt-' + e.detail.callId,
      { width: "640px", height: "480px" }, false);
  }

  hangupHandler(e) {
    console.log('hangupHandler :' + e.detail.callId);
    console.log(e.detail.reason);
    this.deleteMini()
    this.deleteRemote()
  }

  webRTCClientCreatedHandler(e) {
    console.log('webRTCClientCreatedHandler');
  }

  deleteMini() {
    if (document.querySelector('#mini video')) {
      document.querySelector('#mini').removeChild(document.querySelector('#mini video'))
    }
  }

  deleteRemote() {
    if (document.querySelector('#remote video')) {
      document.querySelector('#remote').removeChild(document.querySelector('#remote video'))
    }
  }
}
