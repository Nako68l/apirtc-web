import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CallPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-call',
  templateUrl: 'call.html',
})
export class CallPage {
  webRTCClient;
  apiRTC;
  ionLoad: boolean = false

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    this.webRTCClient = this.navParams.get('webRTCClient')
    this.apiRTC = this.navParams.get('apiRTC')
    this.ionLoad = true;
    this.apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler.bind(this))
    this.apiRTC.addEventListener("remoteStreamAdded", this.remoteStreamAddedHandler.bind(this));
    this.apiRTC.addEventListener("hangup", this.hangupHandler.bind(this));
  }

  hangUp() {
    console.log('out');
    this.apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler.bind(this))
    this.apiRTC.removeEventListener("remoteStreamAdded", this.remoteStreamAddedHandler.bind(this));
    this.apiRTC.removeEventListener("hangup", this.hangupHandler.bind(this));
    this.ionLoad = false;

    this.webRTCClient.hangUp();
    this.navCtrl.pop()
  }

  userMediaSuccessHandler(e) {
    console.log("userMediaSuccessHandler e.detail.callId :" + e.detail.callId);
    console.log("userMediaSuccessHandler e.detail.callType :" + e.detail.callType);
    console.log("userMediaSuccessHandler e.detail.remoteId :" + e.detail.remoteId);
    //Adding local Stream in Div. Video is muted

    //You can decide to manage your own stream display function or use the integrated one of ApiRTC
    /*
        addStreamInDiv(e.detail.stream, e.detail.callType, "mini", 'miniElt-' + e.detail.callId,
                       {width : "160px", height : "120px"}, true);
    */
    this.deleteMini();
    console.log(this.ionLoad);
    if (this.ionLoad) {
      this.webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "cmini", 'miniElt-' + e.detail.callId,
        { width: "128px", height: "96px" }, true);
    }
  }



  remoteStreamAddedHandler(e) {
    console.log("remoteStreamAddedHandler, e.detail.callId :" + e.detail.callId);
    console.log("remoteStreamAddedHandler, e.detail.callType :" + e.detail.callType);
    console.log("userMediaSuccessHandler e.detail.remoteId :" + e.detail.remoteId);

    //Adding Remote Stream in Div. Video is not muted

    //You can decide to manage your own stream display function or use the integrated one of ApiRTC
    /*
        addStreamInDiv(e.detail.stream, e.detail.callType, "remote", 'remoteElt-' + e.detail.callId,
                       {width : "640px", height : "480px"}, false);
    */
    this.deleteRemote()
    console.log(this.ionLoad);
    if (this.ionLoad) {
      this.webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "cremote", 'remoteElt-' + e.detail.callId,
        { width: "100%", height: "calc(100% - 56px)" }, false);
    }
  }

  hangupHandler(e) {
    console.log('hangupHandler :' + e.detail.callId);
    console.log(e.detail.reason);
    this.apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler.bind(this))
    this.apiRTC.removeEventListener("remoteStreamAdded", this.remoteStreamAddedHandler.bind(this));
    this.apiRTC.removeEventListener("hangup", this.hangupHandler.bind(this));
    if(this.ionLoad){
      this.navCtrl.pop()    
    }
    this.ionLoad = false;
  }


  deleteMini() {
    if (document.querySelector('#cmini video')) {
      document.querySelector('#cmini').removeChild(document.querySelector('#cmini video'))
    }
  }

  deleteRemote() {
    if (document.querySelector('#cremote video')) {
      document.querySelector('#cremote').removeChild(document.querySelector('#cremote video'))
    }
  }
}
